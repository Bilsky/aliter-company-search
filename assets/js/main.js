// Register custom handlebars helper
function registerHandlebarsHelpers(){
    Handlebars.registerHelper('ifEqual', function(v1, v2, options) {
        if(v1 === v2) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
}

$( document ).ready(function() {
    registerHandlebarsHelpers();
    var errorTemplate = Handlebars.compile($("#error-template").html());
    var loaderIconTemplate = Handlebars.compile($("#loader-icon-template").html());

    $('[data-toggle="tooltip"]').tooltip();

    //--------------------------------------
    //------- Submit search results --------
    //--------------------------------------
    $('.search-form').on('submit',function(e){
        e.preventDefault();
        var searchResultTemplate = Handlebars.compile($("#search-results-template").html());
        fillCompanyData($(this),searchResultTemplate,errorTemplate,loaderIconTemplate);
        $('.search-results').slideDown();
        $(this).find('.btn-clear').show();
    });
    //--------------------------------------
    //----- END: Submit search results -----
    //--------------------------------------

    //--------------------------------------
    //---------- Expand button ------------
    //--------------------------------------
    $('body').on('click','.expand-button',function(e){
       e.preventDefault();
        $(this).closest('tr').next('.expand-target').toggle();

        var icon = $(this).find('i');
        if(icon.hasClass('glyphicon-chevron-down')){
            icon.removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            $(this).closest('tr').addClass('isExpanded');
        }else{
            icon.removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            $(this).closest('tr').removeClass('isExpanded');
        }
    });
    //--------------------------------------
    //------- END: Expand button ----------
    //--------------------------------------
    
    $('.search-results').on('click','.persons .get-criminal-activity',function(e){
        var criminalActivityTemplate = Handlebars.compile($("#criminal-activity-results-template").html());
        fillCriminalActivity($(this),criminalActivityTemplate,errorTemplate,loaderIconTemplate);
    });

    //--------------------------------------
    //-------- Clear search results --------
    //--------------------------------------
    $('.search-form').on('click','.btn-clear',function(e){
        e.preventDefault();
        var form = $(this).closest('form');
        var button = $(this);
        $(form.data('results-target')).slideUp(function () {
            $(this).find('table').remove();
            $(button.data('criminal-activity-results-target')).html('');
        });
        $(this).closest('form').find('input').val('');
        $(this).hide();
    });
    //--------------------------------------
    //------ END: Clear search results -----
    //--------------------------------------

    //--------------------------------------
    //--------- External services ----------
    //--------------------------------------
    $('body').on('click','.js-simulate-service',function (e) {
        e.preventDefault();
        var button = $(this);
        var resultElement = $(button.data('target'));
        var soapEnvelope = button.data('soap-envelope');
        resultElement.html(loaderIconTemplate());
        $.ajax({
            url: button.data('request-url'),
            type: button.data('request-method'),
            error: function (jqXHR, exception) {
                resultElement.html(errorTemplate());
            },
            success: function(data,textStatus, jqXHR){
                var simulateServiceTemplate = Handlebars.compile($(button.data('response-template')).html());
                var templateData = {};
                if(soapEnvelope) {
                    var xml = $.xml2json(data);
                    templateData = xml['#document']['soap:Envelope']['soap:Body'];
                    templateData['rawResponseData'] = jqXHR['responseText'];
                    resultElement.html(simulateServiceTemplate(templateData));
                }else{
                    templateData = data;
                    templateData['rawResponseData'] = jqXHR['responseText'];
                    resultElement.html(simulateServiceTemplate(templateData));
                }
            }
        });
    });
    $('body').on('click','.js-soap-service-call',function (e) {
        e.preventDefault();
        var button = $(this);
        var resultElement = $(button.data('target'));
        resultElement.html(loaderIconTemplate());
        var soapData = button.data('request-data');
        var findByValue = button.closest('.input-group').find('.js-search-by-parameter-input').val();
        soapData = soapData.replace("{{js-search-by-parameter-input}}", findByValue);
        $.ajax({
            async: true,
            crossDomain: true,
            url: button.data('request-url'),
            method: button.data('request-method'),
            headers: button.data('request-headers'),
            data: soapData,
            error: function (jqXHR, exception) {
                resultElement.html(errorTemplate());
            },
            success: function (data,textStatus, jqXHR) {
                var serviceTemplate = Handlebars.compile($(button.data('response-template')).html());
                var xml = $.xml2json(data);
                var templateData = xml['#document']['soap:Envelope']['soap:Body'];
                templateData['rawResponseData'] = jqXHR['responseText'];

                resultElement.html(serviceTemplate(templateData));
            }
        });
    });

    $('body').on('click','.js-rest-service-call',function (e) {
        e.preventDefault();
        var button = $(this);
        var resultElement = $(button.data('target'));
        resultElement.html(loaderIconTemplate());
        var findByValue = button.closest('.input-group').find('.js-search-by-parameter-input').val();
        var url = button.data('request-url');
        url = url.replace("{{js-search-by-parameter-input}}", findByValue);
        $.ajax({
            async: true,
            crossDomain: true,
            url: url,
            method: button.data('request-method'),
            headers: button.data('request-headers'),
            data: button.data('request-data'),
            error: function (jqXHR, exception) {
                resultElement.html(errorTemplate());
            },
            success: function (data,textStatus, jqXHR) {
                var serviceTemplate = Handlebars.compile($(button.data('response-template')).html());
                var templateData = data;
                templateData['rawResponseData'] = jqXHR['responseText'];

                resultElement.html(serviceTemplate(templateData));
            }
        });
    });
    //-------------------------------------------
    //--------- END: External services ----------
    //-------------------------------------------
});

// Fill company data
function fillCompanyData(form,template,errorTemplate,loaderIconTemplate){
    var searchResultsElement = $(form.data('results-target'));
    searchResultsElement.html(loaderIconTemplate());
    $.ajax({
        url: form.attr('action'),
        data: form.serialize(),
        type: form.attr('method'),
        error: function (jqXHR, exception) {
            searchResultsElement.html(errorTemplate());
        },
        success: function(data){
            searchResultsElement.html(template(data));
        }
    });
}

//Fill criminal activity data
function fillCriminalActivity(button,template,errorTemplate,loaderIconTemplate){
    var criminalActivityResultsElement = $(button.data('results-target'));
    criminalActivityResultsElement.html(loaderIconTemplate());
    $.ajax({
        url: button.data('request-url'),
        type: button.data('request-method'),
        error: function (jqXHR, exception) {
            criminalActivityResultsElement.html(errorTemplate())
        },
        success: function(data){
            criminalActivityResultsElement.html(template(data));
        }
    });
}